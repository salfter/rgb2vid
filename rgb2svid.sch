EESchema Schematic File Version 4
LIBS:rgb2svid-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 3
Title "RGB to Component/S-Video/Composite Converter"
Date "2018-02-22"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L apple2:IIGS_RGB J1
U 1 1 5A8F9E87
P 2500 3900
F 0 "J1" H 2200 4500 60  0000 C CNN
F 1 "IIGS_RGB" H 2875 3275 60  0000 C CNN
F 2 "Connector_Dsub:DSUB-15_Male_Horizontal_P2.77x2.84mm_EdgePinOffset7.70mm_Housed_MountingHolesOffset9.12mm" H 2500 3900 60  0001 C CNN
F 3 "" H 2500 3900 60  0001 C CNN
	1    2500 3900
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C2
U 1 1 5A8F9FD5
P 3300 3450
F 0 "C2" V 3350 3500 50  0000 L CNN
F 1 "0.1u" V 3250 3200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3300 3450 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 3300 3450 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3300 3450
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:C C4
U 1 1 5A8FA149
P 3750 3550
F 0 "C4" V 3800 3600 50  0000 L CNN
F 1 "0.1u" V 3700 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3750 3550 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 3750 3550 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3750 3550
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:C C5
U 1 1 5A8FA239
P 4200 3650
F 0 "C5" V 4250 3700 50  0000 L CNN
F 1 "0.1u" V 4150 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4200 3650 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 4200 3650 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4200 3650
	0    -1   -1   0   
$EndComp
$Comp
L rgb2svid-rescue:L7805 U1
U 1 1 5A8FA5BA
P 3250 2600
F 0 "U1" H 3100 2725 50  0000 C CNN
F 1 "L7805" H 3250 2725 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 3275 2450 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 3250 2550 50  0001 C CNN
F 4 "ST" H 3250 2600 60  0001 C CNN "Manufacturer"
F 5 "497-7255-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 6 "IC REG LINEAR 5V 1.5A DPAK" H 0   0   50  0001 C CNN "Description"
F 7 "L7805CDT-TR" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "https://www.digikey.com/products/en?keywords=497-7255-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "TO-252-2" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3250 2600
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C1
U 1 1 5A8FA68B
P 2850 2800
F 0 "C1" H 2750 2900 50  0000 L CNN
F 1 "0.1u" H 2850 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2850 2800 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 2850 2800 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    2850 2800
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C3
U 1 1 5A8FA724
P 3650 2800
F 0 "C3" H 3550 2900 50  0000 L CNN
F 1 "1u" H 3650 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3650 2800 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F105ZOCNNNC.jsp" H 3650 2800 50  0001 C CNN
F 4 "CAP CER 1UF 16V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F105ZOCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1246-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1246-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3650 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5A8FA7E8
P 3650 3050
F 0 "#PWR01" H 3650 2800 50  0001 C CNN
F 1 "GND" H 3650 2900 50  0000 C CNN
F 2 "" H 3650 3050 50  0001 C CNN
F 3 "" H 3650 3050 50  0001 C CNN
	1    3650 3050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5A8FAA06
P 3650 2550
F 0 "#PWR02" H 3650 2400 50  0001 C CNN
F 1 "VCC" H 3650 2700 50  0000 C CNN
F 2 "" H 3650 2550 50  0001 C CNN
F 3 "" H 3650 2550 50  0001 C CNN
	1    3650 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5A8FAF48
P 2350 4750
F 0 "#PWR03" H 2350 4500 50  0001 C CNN
F 1 "GND" H 2350 4600 50  0000 C CNN
F 2 "" H 2350 4750 50  0001 C CNN
F 3 "" H 2350 4750 50  0001 C CNN
	1    2350 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5A8FAFF8
P 5150 5100
F 0 "#PWR04" H 5150 4850 50  0001 C CNN
F 1 "GND" H 5150 4950 50  0000 C CNN
F 2 "" H 5150 5100 50  0001 C CNN
F 3 "" H 5150 5100 50  0001 C CNN
	1    5150 5100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5A8FB4C7
P 5150 2200
F 0 "#PWR05" H 5150 2050 50  0001 C CNN
F 1 "VCC" H 5150 2350 50  0000 C CNN
F 2 "" H 5150 2200 50  0001 C CNN
F 3 "" H 5150 2200 50  0001 C CNN
	1    5150 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3450 3150 3450
Wire Wire Line
	3450 3450 4650 3450
Wire Wire Line
	3050 3550 3600 3550
Wire Wire Line
	3900 3550 4650 3550
Wire Wire Line
	3050 3650 4050 3650
Wire Wire Line
	4350 3650 4650 3650
Wire Wire Line
	3150 4150 4550 4150
Wire Wire Line
	2500 2600 2500 3150
Wire Wire Line
	2500 2600 2850 2600
Wire Wire Line
	2850 2600 2850 2650
Connection ~ 2850 2600
Wire Wire Line
	3550 2600 3650 2600
Wire Wire Line
	3650 2550 3650 2600
Wire Wire Line
	2850 2950 2850 3000
Wire Wire Line
	2850 3000 3250 3000
Wire Wire Line
	3250 3000 3250 2900
Wire Wire Line
	3650 2950 3650 3000
Connection ~ 3250 3000
Connection ~ 3650 3000
Connection ~ 3650 2600
Wire Wire Line
	5150 5000 5150 5050
Wire Wire Line
	5150 5050 5250 5050
Wire Wire Line
	5250 5050 5250 5000
Connection ~ 5150 5050
$Comp
L proper-passives:R R1
U 1 1 5A8FBED7
P 4450 4350
F 0 "R1" V 4530 4350 50  0000 C CNN
F 1 "3.32k" V 4350 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4380 4350 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 4450 4350 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT3K32" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT3K32CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT3K32CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 3.32K OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4450 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 4350 4600 4350
$Comp
L proper-passives:R R3
U 1 1 5A8FC60C
P 6000 4050
F 0 "R3" V 6050 4150 50  0000 C CNN
F 1 "75" V 5950 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 4050 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 6000 4050 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6000 4050
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R4
U 1 1 5A8FC706
P 6050 4250
F 0 "R4" V 6100 4350 50  0000 C CNN
F 1 "75" V 6000 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5980 4250 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 6050 4250 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6050 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 4050 5850 4050
$Comp
L proper-passives:CP C11
U 1 1 5A8FC7C9
P 6400 4050
F 0 "C11" V 6450 4100 50  0000 L CNN
F 1 "220u" V 6350 3800 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-12_Kemet-T" H 6400 4050 50  0001 C CNN
F 3 "http://datasheets.avx.com/F93.pdf" H 6400 4050 50  0001 C CNN
F 4 "CAP TANT 220UF 6.3V 10% 1411" H 0   0   50  0001 C CNN "Description"
F 5 "1411" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "AVX Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "F930J227KBA" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6400 4050
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:CP C12
U 1 1 5A8FC8F8
P 6450 4250
F 0 "C12" V 6500 4300 50  0000 L CNN
F 1 "220u" V 6400 4000 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-12_Kemet-T" H 6450 4250 50  0001 C CNN
F 3 "http://datasheets.avx.com/F93.pdf" H 6450 4250 50  0001 C CNN
F 4 "CAP TANT 220UF 6.3V 10% 1411" H 0   0   50  0001 C CNN "Description"
F 5 "1411" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "AVX Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "F930J227KBA" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6450 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 4150 5850 4150
Wire Wire Line
	5850 4150 5850 4250
Wire Wire Line
	5850 4250 5900 4250
Wire Wire Line
	6200 4250 6300 4250
Wire Wire Line
	6150 4050 6250 4050
$Comp
L proper-passives:C C7
U 1 1 5A8FCD09
P 5050 2650
F 0 "C7" H 4950 2750 50  0000 L CNN
F 1 "0.01u" H 4850 2550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5050 2650 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F103ZBANNNC.jsp" H 5050 2650 50  0001 C CNN
F 4 "CAP CER 10000PF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F103ZBANNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-2705-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-2705-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5050 2650
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:CP C6
U 1 1 5A8FCE52
P 4800 2650
F 0 "C6" H 4800 2750 50  0000 L CNN
F 1 "470u" H 4600 2550 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-20_Kemet-V" H 4800 2650 50  0001 C CNN
F 3 "http://www.vishay.com/docs/40002/293d.pdf" H 4800 2650 50  0001 C CNN
F 4 "CAP TANT 470UF 6.3V 10% 2917" H 0   0   50  0001 C CNN "Description"
F 5 "2917" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Vishay Sprague" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "293D477X96R3D2TE3" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "718-1749-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=718-1749-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4800 2650
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C8
U 1 1 5A8FD0F2
P 5350 2650
F 0 "C8" H 5250 2750 50  0000 L CNN
F 1 "0.01u" H 5350 2550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5350 2650 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F103ZBANNNC.jsp" H 5350 2650 50  0001 C CNN
F 4 "CAP CER 10000PF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F103ZBANNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-2705-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-2705-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5350 2650
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:CP C9
U 1 1 5A8FD13D
P 5600 2650
F 0 "C9" H 5600 2750 50  0000 L CNN
F 1 "470u" H 5600 2550 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-20_Kemet-V" H 5600 2650 50  0001 C CNN
F 3 "http://www.vishay.com/docs/40002/293d.pdf" H 5600 2650 50  0001 C CNN
F 4 "CAP TANT 470UF 6.3V 10% 2917" H 0   0   50  0001 C CNN "Description"
F 5 "2917" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Vishay Sprague" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "293D477X96R3D2TE3" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "718-1749-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=718-1749-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5600 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2200 5150 2450
Wire Wire Line
	5050 2450 5050 2500
Connection ~ 5150 2450
Wire Wire Line
	4800 2450 4800 2500
Connection ~ 5050 2450
$Comp
L power:GND #PWR06
U 1 1 5A8FD411
P 4800 2850
F 0 "#PWR06" H 4800 2600 50  0001 C CNN
F 1 "GND" H 4800 2700 50  0000 C CNN
F 2 "" H 4800 2850 50  0001 C CNN
F 3 "" H 4800 2850 50  0001 C CNN
	1    4800 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5A8FD452
P 5050 2850
F 0 "#PWR07" H 5050 2600 50  0001 C CNN
F 1 "GND" H 5050 2700 50  0000 C CNN
F 2 "" H 5050 2850 50  0001 C CNN
F 3 "" H 5050 2850 50  0001 C CNN
	1    5050 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5A8FD493
P 5350 2850
F 0 "#PWR08" H 5350 2600 50  0001 C CNN
F 1 "GND" H 5350 2700 50  0000 C CNN
F 2 "" H 5350 2850 50  0001 C CNN
F 3 "" H 5350 2850 50  0001 C CNN
	1    5350 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5A8FD4D4
P 5600 2850
F 0 "#PWR09" H 5600 2600 50  0001 C CNN
F 1 "GND" H 5600 2700 50  0000 C CNN
F 2 "" H 5600 2850 50  0001 C CNN
F 3 "" H 5600 2850 50  0001 C CNN
	1    5600 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2800 4800 2850
Wire Wire Line
	5050 2850 5050 2800
Wire Wire Line
	5350 2800 5350 2850
Wire Wire Line
	5600 2850 5600 2800
Wire Wire Line
	4800 2450 5050 2450
$Comp
L power:VCC #PWR010
U 1 1 5A8FD754
P 5250 2300
F 0 "#PWR010" H 5250 2150 50  0001 C CNN
F 1 "VCC" H 5250 2450 50  0000 C CNN
F 2 "" H 5250 2300 50  0001 C CNN
F 3 "" H 5250 2300 50  0001 C CNN
	1    5250 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2300 5250 2450
Wire Wire Line
	5250 2450 5350 2450
Wire Wire Line
	5350 2450 5350 2500
Connection ~ 5250 2450
Wire Wire Line
	5600 2450 5600 2500
Connection ~ 5350 2450
$Comp
L cxa1645:CXA2075 U2
U 1 1 5A8FDC56
P 5200 4000
F 0 "U2" H 4900 4850 60  0000 C CNN
F 1 "CXA2075" H 5475 3100 60  0000 C CNN
F 2 "sop:SOP-24_15.0x5.3mm_P1.27mm" H 5050 3975 60  0001 C CNN
F 3 "http://pdf1.alldatasheet.com/datasheet-pdf/view/146350/SONY/CXA2075M/+0_5473VPMMZb.a.NBEDU+/datasheet.pdf" H 5050 3975 60  0001 C CNN
F 4 "RGB Encoder" H 0   0   50  0001 C CNN "Description"
F 5 "Sony" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CXA2075M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "n" H 0   0   50  0001 C CNN "Subs OK?"
	1    5200 4000
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:Mini-DIN-4 J3
U 1 1 5A8FDFCE
P 7500 4200
F 0 "J3" H 7500 4450 50  0000 C CNN
F 1 "S-Video" H 7500 3950 50  0000 C CNN
F 2 "conns:mini-din-4" H 7500 4200 50  0001 C CNN
F 3 "https://www.cui.com/product/resource/md-s.pdf" H 7500 4200 50  0001 C CNN
F 4 "CUI Inc." H 7500 4200 50  0001 C CNN "Manufacturer"
F 5 "MD-40S" H 7500 4200 50  0001 C CNN "Mfr PN"
F 6 "1" H 7500 4200 50  0001 C CNN "Qty Per Unit"
F 7 "CP-2440-ND" H 7500 4200 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=CP-2440-ND" H 7500 4200 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H 7500 4200 50  0001 C CNN "Subs OK?"
F 10 "https://www.digikey.com/products/en?keywords=CP-2440-ND" H 7500 4200 50  0001 C CNN "Description"
	1    7500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4250 6900 4250
Wire Wire Line
	6900 4250 6900 4100
Wire Wire Line
	6900 4100 7200 4100
Wire Wire Line
	6550 4050 7150 4050
Wire Wire Line
	7150 4050 7150 3900
Wire Wire Line
	7150 3900 7900 3900
Wire Wire Line
	7900 3900 7900 4100
Wire Wire Line
	7900 4100 7800 4100
$Comp
L power:GND #PWR011
U 1 1 5A8FE1A1
P 7150 4600
F 0 "#PWR011" H 7150 4350 50  0001 C CNN
F 1 "GND" H 7150 4450 50  0000 C CNN
F 2 "" H 7150 4600 50  0001 C CNN
F 3 "" H 7150 4600 50  0001 C CNN
	1    7150 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4200 7150 4200
Wire Wire Line
	7150 4200 7150 4500
Wire Wire Line
	7150 4500 7900 4500
Wire Wire Line
	7900 4500 7900 4200
Wire Wire Line
	7900 4200 7800 4200
Connection ~ 7150 4500
$Comp
L proper-passives:R R2
U 1 1 5A8FE2F9
P 6000 3850
F 0 "R2" V 6050 3950 50  0000 C CNN
F 1 "75" V 5950 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 3850 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 6000 3850 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6000 3850
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:CP C10
U 1 1 5A8FE349
P 6400 3850
F 0 "C10" V 6450 3900 50  0000 L CNN
F 1 "220u" V 6350 3600 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-12_Kemet-T" H 6400 3850 50  0001 C CNN
F 3 "http://datasheets.avx.com/F93.pdf" H 6400 3850 50  0001 C CNN
F 4 "CAP TANT 220UF 6.3V 10% 1411" H 0   0   50  0001 C CNN "Description"
F 5 "1411" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "AVX Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "F930J227KBA" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=478-8166-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6400 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 3850 5850 3850
Wire Wire Line
	6150 3850 6250 3850
$Comp
L rgb2svid-rescue:Conn_Coaxial J2
U 1 1 5A8FE45C
P 7500 3400
F 0 "J2" H 7510 3520 50  0000 C CNN
F 1 "Composite" H 7700 3300 50  0000 C CNN
F 2 "conns:rca-right-angle" H 7500 3400 50  0001 C CNN
F 3 "http://www.switchcraft.com/Drawings/pjran1x1u__x_series_cd.pdf" H 7500 3400 50  0001 C CNN
F 4 "Switchcraft Inc." H 7500 3400 50  0001 C CNN "Manufacturer"
F 5 "PJRAN1X1U04X" H 7500 3400 50  0001 C CNN "Mfr PN"
F 6 "1" H 7500 3400 50  0001 C CNN "Qty Per Unit"
F 7 "PJRAN1X1U04X-ND" H 7500 3400 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=PJRAN1X1U04X-ND" H 7500 3400 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H 7500 3400 50  0001 C CNN "Subs OK?"
F 10 "CONN RCA JACK MONO 3.2MM R/A" H 7500 3400 50  0001 C CNN "Description"
	1    7500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3850 6550 3850
Wire Wire Line
	7050 3400 7050 3850
$Comp
L power:GND #PWR012
U 1 1 5A8FE58C
P 7500 3650
F 0 "#PWR012" H 7500 3400 50  0001 C CNN
F 1 "GND" H 7500 3500 50  0000 C CNN
F 2 "" H 7500 3650 50  0001 C CNN
F 3 "" H 7500 3650 50  0001 C CNN
	1    7500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3650 7500 3600
Wire Wire Line
	7350 3400 7050 3400
$Comp
L power:VEE #PWR013
U 1 1 5A8FD676
P 2650 4750
F 0 "#PWR013" H 2650 4600 50  0001 C CNN
F 1 "VEE" H 2650 4900 50  0000 C CNN
F 2 "" H 2650 4750 50  0001 C CNN
F 3 "" H 2650 4750 50  0001 C CNN
	1    2650 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 4650 2650 4750
$Sheet
S 6600 2350 500  500 
U 5A8FE7B6
F0 "rgb2component" 60
F1 "rgb2component.sch" 60
F2 "Red" I L 6600 2450 60 
F3 "Green" I L 6600 2550 60 
F4 "Blue" I L 6600 2650 60 
F5 "Sync" I L 6600 2750 60 
$EndSheet
Wire Wire Line
	4550 4150 4550 2350
Wire Wire Line
	4550 2350 6000 2350
Wire Wire Line
	6000 2350 6000 2750
Wire Wire Line
	6000 2750 6600 2750
Connection ~ 4550 4150
NoConn ~ 5750 4350
NoConn ~ 3050 3950
NoConn ~ 3050 4150
Wire Wire Line
	2350 4650 2350 4700
Wire Wire Line
	5750 3450 6100 3450
Wire Wire Line
	6100 3450 6100 2450
Wire Wire Line
	6100 2450 6600 2450
Wire Wire Line
	5750 3550 6200 3550
Wire Wire Line
	6200 3550 6200 2550
Wire Wire Line
	6200 2550 6600 2550
Wire Wire Line
	5750 3650 6300 3650
Wire Wire Line
	6300 3650 6300 2650
Wire Wire Line
	6300 2650 6600 2650
Wire Wire Line
	2350 4700 2450 4700
Wire Wire Line
	2450 4700 2450 4650
Connection ~ 2350 4700
Wire Wire Line
	2550 4700 2550 4650
Connection ~ 2450 4700
Text Label 2500 2600 2    60   ~ 0
+12V
$Comp
L via:via U4
U 1 1 5A934273
P 5600 5300
F 0 "U4" H 5600 5500 60  0001 C CNN
F 1 "via" H 5600 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 5600 5300 60  0001 C CNN
F 3 "" H 5600 5300 60  0001 C CNN
	1    5600 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5A9342CB
P 5600 5350
F 0 "#PWR014" H 5600 5100 50  0001 C CNN
F 1 "GND" H 5600 5200 50  0000 C CNN
F 2 "" H 5600 5350 50  0001 C CNN
F 3 "" H 5600 5350 50  0001 C CNN
	1    5600 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 5300 5600 5350
$Comp
L via:via U5
U 1 1 5A934476
P 5750 5300
F 0 "U5" H 5750 5500 60  0001 C CNN
F 1 "via" H 5750 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 5750 5300 60  0001 C CNN
F 3 "" H 5750 5300 60  0001 C CNN
	1    5750 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5A93447C
P 5750 5350
F 0 "#PWR015" H 5750 5100 50  0001 C CNN
F 1 "GND" H 5750 5200 50  0000 C CNN
F 2 "" H 5750 5350 50  0001 C CNN
F 3 "" H 5750 5350 50  0001 C CNN
	1    5750 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5300 5750 5350
$Comp
L via:via U6
U 1 1 5A93450B
P 5900 5300
F 0 "U6" H 5900 5500 60  0001 C CNN
F 1 "via" H 5900 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 5900 5300 60  0001 C CNN
F 3 "" H 5900 5300 60  0001 C CNN
	1    5900 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5A934511
P 5900 5350
F 0 "#PWR016" H 5900 5100 50  0001 C CNN
F 1 "GND" H 5900 5200 50  0000 C CNN
F 2 "" H 5900 5350 50  0001 C CNN
F 3 "" H 5900 5350 50  0001 C CNN
	1    5900 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5300 5900 5350
$Comp
L via:via U7
U 1 1 5A934518
P 6050 5300
F 0 "U7" H 6050 5500 60  0001 C CNN
F 1 "via" H 6050 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6050 5300 60  0001 C CNN
F 3 "" H 6050 5300 60  0001 C CNN
	1    6050 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5A93451E
P 6050 5350
F 0 "#PWR017" H 6050 5100 50  0001 C CNN
F 1 "GND" H 6050 5200 50  0000 C CNN
F 2 "" H 6050 5350 50  0001 C CNN
F 3 "" H 6050 5350 50  0001 C CNN
	1    6050 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5300 6050 5350
$Comp
L via:via U8
U 1 1 5A93504D
P 6200 5300
F 0 "U8" H 6200 5500 60  0001 C CNN
F 1 "via" H 6200 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6200 5300 60  0001 C CNN
F 3 "" H 6200 5300 60  0001 C CNN
	1    6200 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5A935053
P 6200 5350
F 0 "#PWR018" H 6200 5100 50  0001 C CNN
F 1 "GND" H 6200 5200 50  0000 C CNN
F 2 "" H 6200 5350 50  0001 C CNN
F 3 "" H 6200 5350 50  0001 C CNN
	1    6200 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5300 6200 5350
$Comp
L via:via U9
U 1 1 5A93505A
P 6350 5300
F 0 "U9" H 6350 5500 60  0001 C CNN
F 1 "via" H 6350 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6350 5300 60  0001 C CNN
F 3 "" H 6350 5300 60  0001 C CNN
	1    6350 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5A935060
P 6350 5350
F 0 "#PWR019" H 6350 5100 50  0001 C CNN
F 1 "GND" H 6350 5200 50  0000 C CNN
F 2 "" H 6350 5350 50  0001 C CNN
F 3 "" H 6350 5350 50  0001 C CNN
	1    6350 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5300 6350 5350
$Comp
L via:via U10
U 1 1 5A935067
P 6500 5300
F 0 "U10" H 6500 5500 60  0001 C CNN
F 1 "via" H 6500 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6500 5300 60  0001 C CNN
F 3 "" H 6500 5300 60  0001 C CNN
	1    6500 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5A93506D
P 6500 5350
F 0 "#PWR020" H 6500 5100 50  0001 C CNN
F 1 "GND" H 6500 5200 50  0000 C CNN
F 2 "" H 6500 5350 50  0001 C CNN
F 3 "" H 6500 5350 50  0001 C CNN
	1    6500 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5300 6500 5350
$Comp
L via:via U11
U 1 1 5A935074
P 6650 5300
F 0 "U11" H 6650 5500 60  0001 C CNN
F 1 "via" H 6650 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6650 5300 60  0001 C CNN
F 3 "" H 6650 5300 60  0001 C CNN
	1    6650 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5A93507A
P 6650 5350
F 0 "#PWR021" H 6650 5100 50  0001 C CNN
F 1 "GND" H 6650 5200 50  0000 C CNN
F 2 "" H 6650 5350 50  0001 C CNN
F 3 "" H 6650 5350 50  0001 C CNN
	1    6650 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5300 6650 5350
$Comp
L via:via U12
U 1 1 5A935109
P 6800 5300
F 0 "U12" H 6800 5500 60  0001 C CNN
F 1 "via" H 6800 5400 60  0000 C CNN
F 2 "thermal-vias:via-25mil" H 6800 5300 60  0001 C CNN
F 3 "" H 6800 5300 60  0001 C CNN
	1    6800 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5A93510F
P 6800 5350
F 0 "#PWR022" H 6800 5100 50  0001 C CNN
F 1 "GND" H 6800 5200 50  0000 C CNN
F 2 "" H 6800 5350 50  0001 C CNN
F 3 "" H 6800 5350 50  0001 C CNN
	1    6800 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5300 6800 5350
$Sheet
S 3250 3750 550  200 
U 5A932E50
F0 "oscillator" 60
F1 "osc.sch" 60
F2 "XOUT" O R 3800 3850 60 
$EndSheet
Wire Wire Line
	3150 4150 3150 3750
Wire Wire Line
	3150 3750 3050 3750
$Comp
L power:VCC #PWR023
U 1 1 5A8FBE34
P 3700 4400
F 0 "#PWR023" H 3700 4250 50  0001 C CNN
F 1 "VCC" H 3700 4550 50  0000 C CNN
F 2 "" H 3700 4400 50  0001 C CNN
F 3 "" H 3700 4400 50  0001 C CNN
	1    3700 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP1
U 1 1 5A9363C5
P 3800 4400
F 0 "JP1" H 3800 4480 50  0000 C CNN
F 1 "Jumper_NC_Small" H 3810 4340 50  0001 C CNN
F 2 "jumpers:jumper_nc" H 3800 4400 50  0001 C CNN
F 3 "" H 3800 4400 50  0001 C CNN
	1    3800 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 5A9366D3
P 3800 4450
F 0 "JP2" H 3800 4400 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3810 4390 50  0001 C CNN
F 2 "jumpers:jumper_no" H 3800 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0001 C CNN
	1    3800 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5A936923
P 3700 4450
F 0 "#PWR024" H 3700 4200 50  0001 C CNN
F 1 "GND" H 3700 4300 50  0000 C CNN
F 2 "" H 3700 4450 50  0001 C CNN
F 3 "" H 3700 4450 50  0001 C CNN
	1    3700 4450
	1    0    0    -1  
$EndComp
Text Notes 4200 4650 0    50   ~ 0
(NTSC; 2.61k\nfor PAL)
$Comp
L power:VCC #PWR025
U 1 1 5A936BF7
P 4300 4350
F 0 "#PWR025" H 4300 4200 50  0001 C CNN
F 1 "VCC" H 4300 4500 50  0000 C CNN
F 2 "" H 4300 4350 50  0001 C CNN
F 3 "" H 4300 4350 50  0001 C CNN
	1    4300 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3850 4650 3850
Wire Wire Line
	4650 3950 4000 3950
Wire Wire Line
	4000 3950 4000 4400
Wire Wire Line
	4000 4400 3950 4400
Wire Wire Line
	3900 4450 3950 4450
Wire Wire Line
	3950 4450 3950 4400
Connection ~ 3950 4400
$Comp
L via:via U14
U 1 1 5A9380C6
P 2900 5250
F 0 "U14" H 2900 5450 60  0001 C CNN
F 1 "via" H 2900 5350 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 2900 5250 60  0001 C CNN
F 3 "" H 2900 5250 60  0001 C CNN
	1    2900 5250
	1    0    0    -1  
$EndComp
$Comp
L via:via U15
U 1 1 5A9384DB
P 2900 5400
F 0 "U15" H 2900 5600 60  0001 C CNN
F 1 "via" H 2900 5500 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 2900 5400 60  0001 C CNN
F 3 "" H 2900 5400 60  0001 C CNN
	1    2900 5400
	1    0    0    -1  
$EndComp
$Comp
L via:via U16
U 1 1 5A938549
P 2900 5550
F 0 "U16" H 2900 5750 60  0001 C CNN
F 1 "via" H 2900 5650 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 2900 5550 60  0001 C CNN
F 3 "" H 2900 5550 60  0001 C CNN
	1    2900 5550
	1    0    0    -1  
$EndComp
$Comp
L via:via U17
U 1 1 5A9385B7
P 3200 5250
F 0 "U17" H 3200 5450 60  0001 C CNN
F 1 "via" H 3200 5350 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 3200 5250 60  0001 C CNN
F 3 "" H 3200 5250 60  0001 C CNN
	1    3200 5250
	1    0    0    -1  
$EndComp
$Comp
L via:via U18
U 1 1 5A938625
P 3200 5400
F 0 "U18" H 3200 5600 60  0001 C CNN
F 1 "via" H 3200 5500 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 3200 5400 60  0001 C CNN
F 3 "" H 3200 5400 60  0001 C CNN
	1    3200 5400
	1    0    0    -1  
$EndComp
$Comp
L via:via U19
U 1 1 5A938693
P 3200 5550
F 0 "U19" H 3200 5750 60  0001 C CNN
F 1 "via" H 3200 5650 60  0000 C CNN
F 2 "untented-vias:via-untented-15-25" H 3200 5550 60  0001 C CNN
F 3 "" H 3200 5550 60  0001 C CNN
	1    3200 5550
	1    0    0    -1  
$EndComp
Text Label 4400 3650 0    60   ~ 0
B_IN
Text Label 4350 3550 0    60   ~ 0
G_IN
Text Label 4300 3450 0    60   ~ 0
R_IN
Text Label 5800 3450 0    60   ~ 0
R_OUT
Text Label 5850 3550 0    60   ~ 0
G_OUT
Text Label 5950 3650 0    60   ~ 0
B_OUT
Text Label 2900 5250 0    60   ~ 0
R_IN
Text Label 2900 5400 0    60   ~ 0
G_IN
Text Label 2900 5550 0    60   ~ 0
B_IN
Text Label 3200 5250 0    60   ~ 0
R_OUT
Text Label 3200 5400 0    60   ~ 0
G_OUT
Text Label 3200 5550 0    60   ~ 0
B_OUT
Wire Wire Line
	2850 2600 2950 2600
Wire Wire Line
	3250 3000 3650 3000
Wire Wire Line
	3650 3000 3650 3050
Wire Wire Line
	3650 2600 3650 2650
Wire Wire Line
	5150 5050 5150 5100
Wire Wire Line
	5150 2450 5150 3000
Wire Wire Line
	5050 2450 5150 2450
Wire Wire Line
	5250 2450 5250 3000
Wire Wire Line
	5350 2450 5600 2450
Wire Wire Line
	7150 4500 7150 4600
Wire Wire Line
	4550 4150 4650 4150
Wire Wire Line
	2350 4700 2350 4750
Wire Wire Line
	2450 4700 2550 4700
Wire Wire Line
	3950 4400 3900 4400
$EndSCHEMATC
