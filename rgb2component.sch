EESchema Schematic File Version 4
LIBS:rgb2svid-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 3
Title "RGB to Component/S-Video/Composite Converter"
Date "2018-02-22"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2800 2300 0    60   Input ~ 0
Red
Text HLabel 2800 3450 0    60   Input ~ 0
Green
Text HLabel 2800 4600 0    60   Input ~ 0
Blue
Text HLabel 2800 5550 0    60   Input ~ 0
Sync
$Comp
L proper-passives:CP C13
U 1 1 5A90032F
P 3050 5550
F 0 "C13" V 3000 5400 50  0000 L CNN
F 1 "100u" V 3100 5600 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-12_Kemet-T" H 3050 5550 50  0001 C CNN
F 3 "http://datasheets.avx.com/F93.pdf" H 3050 5550 50  0001 C CNN
F 4 "CAP TANT 100UF 6.3V 20% 1411" H 0   0   50  0001 C CNN "Description"
F 5 "1411" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "AVX Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "F930J107MBA" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "478-8156-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=478-8156-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3050 5550
	0    1    1    0   
$EndComp
$Comp
L proper-passives:R R5
U 1 1 5A9003AE
P 3050 3450
F 0 "R5" V 3130 3450 50  0000 C CNN
F 1 "680" V 2950 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2980 3450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 3050 3450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT680R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT680RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT680RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 680 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3050 3450
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R7
U 1 1 5A9003EF
P 3550 3600
F 0 "R7" V 3630 3600 50  0000 C CNN
F 1 "82" V 3450 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 3600 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 3550 3600 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT82R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT82R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT82R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 82 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3550 3600
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R8
U 1 1 5A9004B4
P 3550 4750
F 0 "R8" V 3630 4750 50  0000 C CNN
F 1 "91" V 3450 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 4750 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 3550 4750 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT91R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT91R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT91R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 91 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3550 4750
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R6
U 1 1 5A90058B
P 3550 2450
F 0 "R6" V 3630 2450 50  0000 C CNN
F 1 "91" V 3450 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 2450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 3550 2450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT91R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT91R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT91R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 91 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3550 2450
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R9
U 1 1 5A900666
P 3850 3450
F 0 "R9" V 3930 3450 50  0000 C CNN
F 1 "866" V 3750 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3780 3450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 3850 3450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." V 3850 3450 60  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT866R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT866RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT866RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 866 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3850 3450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5A9006C2
P 3550 2650
F 0 "#PWR026" H 3550 2400 50  0001 C CNN
F 1 "GND" H 3550 2500 50  0000 C CNN
F 2 "" H 3550 2650 50  0001 C CNN
F 3 "" H 3550 2650 50  0001 C CNN
	1    3550 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5A9006E6
P 3550 3800
F 0 "#PWR027" H 3550 3550 50  0001 C CNN
F 1 "GND" H 3550 3650 50  0000 C CNN
F 2 "" H 3550 3800 50  0001 C CNN
F 3 "" H 3550 3800 50  0001 C CNN
	1    3550 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5A900703
P 3550 4950
F 0 "#PWR028" H 3550 4700 50  0001 C CNN
F 1 "GND" H 3550 4800 50  0000 C CNN
F 2 "" H 3550 4950 50  0001 C CNN
F 3 "" H 3550 4950 50  0001 C CNN
	1    3550 4950
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:R R10
U 1 1 5A900756
P 4050 2450
F 0 "R10" V 4130 2450 50  0000 C CNN
F 1 "1.69k" V 3950 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3980 2450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 4050 2450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." V 4050 2450 60  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT1K69" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT1K69CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT1K69CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 1.69K OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4050 2450
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R11
U 1 1 5A9007CE
P 4050 4450
F 0 "R11" V 4130 4450 50  0000 C CNN
F 1 "4.64k" V 3950 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3980 4450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 4050 4450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." V 4050 4450 60  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT4K64" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT4K64CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT4K64CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 4.64K OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4050 4450
	-1   0    0    1   
$EndComp
$Comp
L rgb2svid-rescue:MC33079 U3
U 1 1 5A90087A
P 4700 3350
F 0 "U3" H 4700 3550 50  0000 L CNN
F 1 "MAX4383" H 4700 3150 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4650 3450 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX4380-MAX4384.pdf" H 4750 3550 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC OPAMP VFB 210MHZ RRO 14SOIC" H 0   0   50  0001 C CNN "Description"
F 6 "Maxim Integrated" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MAX4383ESD+" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "n" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4700 3350
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:MC33079 U3
U 2 1 5A900910
P 6150 3250
F 0 "U3" H 6150 3450 50  0000 L CNN
F 1 "MAX4383" H 6150 3050 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6100 3350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX4380-MAX4384.pdf" H 6200 3450 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC OPAMP VFB 210MHZ RRO 14SOIC" H 0   0   50  0001 C CNN "Description"
F 6 "Maxim Integrated" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MAX4383ESD+" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "n" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	2    6150 3250
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:MC33079 U3
U 4 1 5A9009B4
P 7000 2400
F 0 "U3" H 7000 2600 50  0000 L CNN
F 1 "MAX4383" H 7000 2200 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6950 2500 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX4380-MAX4384.pdf" H 7050 2600 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC OPAMP VFB 210MHZ RRO 14SOIC" H 0   0   50  0001 C CNN "Description"
F 6 "Maxim Integrated" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MAX4383ESD+" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "n" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	4    7000 2400
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:MC33079 U3
U 3 1 5A900A07
P 6950 4700
F 0 "U3" H 6950 4900 50  0000 L CNN
F 1 "MAX4383" H 6950 4500 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6900 4800 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX4380-MAX4384.pdf" H 7000 4900 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC OPAMP VFB 210MHZ RRO 14SOIC" H 0   0   50  0001 C CNN "Description"
F 6 "Maxim Integrated" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MAX4383ESD+" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "n" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=MAX4383ESD+-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	3    6950 4700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR029
U 1 1 5A900B67
P 4600 2900
F 0 "#PWR029" H 4600 2750 50  0001 C CNN
F 1 "VCC" H 4600 3050 50  0000 C CNN
F 2 "" H 4600 2900 50  0001 C CNN
F 3 "" H 4600 2900 50  0001 C CNN
	1    4600 2900
	1    0    0    -1  
$EndComp
$Comp
L power:VEE #PWR030
U 1 1 5A900BF2
P 4600 3700
F 0 "#PWR030" H 4600 3550 50  0001 C CNN
F 1 "VEE" H 4600 3850 50  0000 C CNN
F 2 "" H 4600 3700 50  0001 C CNN
F 3 "" H 4600 3700 50  0001 C CNN
	1    4600 3700
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R14
U 1 1 5A900C6B
P 5050 3150
F 0 "R14" V 5130 3150 50  0000 C CNN
F 1 "510" V 4950 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4980 3150 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 5050 3150 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5050 3150
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:C C14
U 1 1 5A900E2E
P 5050 5450
F 0 "C14" H 5075 5550 50  0000 L CNN
F 1 "0.1u" H 5075 5350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5050 5450 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 5050 5450 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5050 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR031
U 1 1 5A900F2D
P 4850 5700
F 0 "#PWR031" H 4850 5450 50  0001 C CNN
F 1 "GND" H 4850 5550 50  0000 C CNN
F 2 "" H 4850 5700 50  0001 C CNN
F 3 "" H 4850 5700 50  0001 C CNN
	1    4850 5700
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:R R13
U 1 1 5A900F8D
P 4650 4600
F 0 "R13" V 4730 4600 50  0000 C CNN
F 1 "510" V 4550 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4580 4600 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 4650 4600 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4650 4600
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R12
U 1 1 5A901016
P 4650 2300
F 0 "R12" V 4730 2300 50  0000 C CNN
F 1 "510" V 4550 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4580 2300 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 4650 2300 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4650 2300
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R15
U 1 1 5A901112
P 5250 2450
F 0 "R15" V 5330 2450 50  0000 C CNN
F 1 "510" V 5150 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5180 2450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 5250 2450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5250 2450
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R16
U 1 1 5A90116F
P 5250 4450
F 0 "R16" V 5330 4450 50  0000 C CNN
F 1 "510" V 5150 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5180 4450 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 5250 4450 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5250 4450
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R17
U 1 1 5A9012C5
P 5500 3350
F 0 "R17" V 5580 3350 50  0000 C CNN
F 1 "510" V 5400 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 3350 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 5500 3350 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5500 3350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5A901435
P 5700 3450
F 0 "#PWR032" H 5700 3200 50  0001 C CNN
F 1 "GND" H 5700 3300 50  0000 C CNN
F 2 "" H 5700 3450 50  0001 C CNN
F 3 "" H 5700 3450 50  0001 C CNN
	1    5700 3450
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:R R18
U 1 1 5A9019DC
P 6500 3100
F 0 "R18" V 6580 3100 50  0000 C CNN
F 1 "510" V 6400 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6430 3100 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 6500 3100 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6500 3100
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R21
U 1 1 5A903A6B
P 7500 2550
F 0 "R21" V 7580 2550 50  0000 C CNN
F 1 "510" V 7400 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7430 2550 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7500 2550 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7500 2550
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R22
U 1 1 5A903B22
P 7500 2950
F 0 "R22" V 7580 2950 50  0000 C CNN
F 1 "510" V 7400 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7430 2950 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7500 2950 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7500 2950
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R19
U 1 1 5A90453A
P 7450 4850
F 0 "R19" V 7530 4850 50  0000 C CNN
F 1 "510" V 7350 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7380 4850 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7450 4850 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7450 4850
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R20
U 1 1 5A9045DB
P 7450 5250
F 0 "R20" V 7530 5250 50  0000 C CNN
F 1 "510" V 7350 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7380 5250 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7450 5250 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT510R" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT510RCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 510 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7450 5250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5A904AA5
P 7450 5450
F 0 "#PWR033" H 7450 5200 50  0001 C CNN
F 1 "GND" H 7450 5300 50  0000 C CNN
F 2 "" H 7450 5450 50  0001 C CNN
F 3 "" H 7450 5450 50  0001 C CNN
	1    7450 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5A904AF8
P 7500 3150
F 0 "#PWR034" H 7500 2900 50  0001 C CNN
F 1 "GND" H 7500 3000 50  0000 C CNN
F 2 "" H 7500 3150 50  0001 C CNN
F 3 "" H 7500 3150 50  0001 C CNN
	1    7500 3150
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C16
U 1 1 5A904E08
P 5300 5450
F 0 "C16" H 5300 5550 50  0000 L CNN
F 1 "10u" H 5300 5350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5300 5450 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F106ZPFNNNE.jsp" H 5300 5450 50  0001 C CNN
F 4 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "CAP CER 10UF 10V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F106ZPFNNNE" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-3012-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-3012-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5300 5450
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C17
U 1 1 5A905259
P 5300 5850
F 0 "C17" H 5300 5950 50  0000 L CNN
F 1 "10u" H 5300 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5300 5850 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F106ZPFNNNE.jsp" H 5300 5850 50  0001 C CNN
F 4 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "CAP CER 10UF 10V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F106ZPFNNNE" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-3012-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-3012-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5300 5850
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C15
U 1 1 5A9052CB
P 5050 5850
F 0 "C15" H 5075 5950 50  0000 L CNN
F 1 "0.1u" H 5075 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5050 5850 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 5050 5850 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5050 5850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR035
U 1 1 5A905395
P 5050 5200
F 0 "#PWR035" H 5050 5050 50  0001 C CNN
F 1 "VCC" H 5050 5350 50  0000 C CNN
F 2 "" H 5050 5200 50  0001 C CNN
F 3 "" H 5050 5200 50  0001 C CNN
	1    5050 5200
	1    0    0    -1  
$EndComp
$Comp
L power:VEE #PWR036
U 1 1 5A9053F1
P 5300 6100
F 0 "#PWR036" H 5300 5950 50  0001 C CNN
F 1 "VEE" H 5300 6250 50  0000 C CNN
F 2 "" H 5300 6100 50  0001 C CNN
F 3 "" H 5300 6100 50  0001 C CNN
	1    5300 6100
	-1   0    0    1   
$EndComp
$Comp
L proper-passives:R R24
U 1 1 5A905FEE
P 7750 2400
F 0 "R24" V 7830 2400 50  0000 C CNN
F 1 "75" V 7650 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7680 2400 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7750 2400 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7750 2400
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R25
U 1 1 5A90613E
P 7750 3500
F 0 "R25" V 7830 3500 50  0000 C CNN
F 1 "75" V 7650 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7680 3500 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7750 3500 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7750 3500
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:R R23
U 1 1 5A9061D5
P 7700 4700
F 0 "R23" V 7780 4700 50  0000 C CNN
F 1 "75" V 7600 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7630 4700 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 7700 4700 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT75R0" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT75R0CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 75 OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7700 4700
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR037
U 1 1 5A9063A1
P 6050 2850
F 0 "#PWR037" H 6050 2700 50  0001 C CNN
F 1 "VCC" H 6050 3000 50  0000 C CNN
F 2 "" H 6050 2850 50  0001 C CNN
F 3 "" H 6050 2850 50  0001 C CNN
	1    6050 2850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR038
U 1 1 5A906406
P 6900 2050
F 0 "#PWR038" H 6900 1900 50  0001 C CNN
F 1 "VCC" H 6900 2200 50  0000 C CNN
F 2 "" H 6900 2050 50  0001 C CNN
F 3 "" H 6900 2050 50  0001 C CNN
	1    6900 2050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR039
U 1 1 5A90646B
P 6850 4350
F 0 "#PWR039" H 6850 4200 50  0001 C CNN
F 1 "VCC" H 6850 4500 50  0000 C CNN
F 2 "" H 6850 4350 50  0001 C CNN
F 3 "" H 6850 4350 50  0001 C CNN
	1    6850 4350
	1    0    0    -1  
$EndComp
$Comp
L power:VEE #PWR040
U 1 1 5A9064D0
P 6050 3650
F 0 "#PWR040" H 6050 3500 50  0001 C CNN
F 1 "VEE" H 6050 3800 50  0000 C CNN
F 2 "" H 6050 3650 50  0001 C CNN
F 3 "" H 6050 3650 50  0001 C CNN
	1    6050 3650
	-1   0    0    1   
$EndComp
$Comp
L power:VEE #PWR041
U 1 1 5A906535
P 6900 2800
F 0 "#PWR041" H 6900 2650 50  0001 C CNN
F 1 "VEE" H 6900 2950 50  0000 C CNN
F 2 "" H 6900 2800 50  0001 C CNN
F 3 "" H 6900 2800 50  0001 C CNN
	1    6900 2800
	-1   0    0    1   
$EndComp
$Comp
L power:VEE #PWR042
U 1 1 5A90659A
P 6850 5150
F 0 "#PWR042" H 6850 5000 50  0001 C CNN
F 1 "VEE" H 6850 5300 50  0000 C CNN
F 2 "" H 6850 5150 50  0001 C CNN
F 3 "" H 6850 5150 50  0001 C CNN
	1    6850 5150
	-1   0    0    1   
$EndComp
$Comp
L triple_coax:Triple_Coax J?
U 1 1 5A903F9F
P 8350 3500
AR Path="/5A903F9F" Ref="J?"  Part="1" 
AR Path="/5A8FE7B6/5A903F9F" Ref="J4"  Part="1" 
F 0 "J4" H 8350 3950 50  0000 C CNN
F 1 "Triple_Coax" V 8465 3500 50  0000 C CNN
F 2 "conns:triple-coax" H 8350 3500 50  0001 C CNN
F 3 "" H 8350 3500 50  0001 C CNN
F 4 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 5 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 6 "footprint is for a part obtained from a random AliExpress seller; can probably substitute one each of CUI RCJ-042/045/046" H 8350 3500 50  0001 C CNN "Note"
	1    8350 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5A904718
P 8350 4150
F 0 "#PWR043" H 8350 3900 50  0001 C CNN
F 1 "GND" H 8350 4000 50  0000 C CNN
F 2 "" H 8350 4150 50  0001 C CNN
F 3 "" H 8350 4150 50  0001 C CNN
	1    8350 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR044
U 1 1 5A907F4F
P 4350 3500
F 0 "#PWR044" H 4350 3250 50  0001 C CNN
F 1 "GND" H 4350 3350 50  0000 C CNN
F 2 "" H 4350 3500 50  0001 C CNN
F 3 "" H 4350 3500 50  0001 C CNN
	1    4350 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5550 2900 5550
Wire Wire Line
	2800 3450 2900 3450
Wire Wire Line
	3200 3450 3300 3450
Wire Wire Line
	2800 4600 3550 4600
Wire Wire Line
	3300 5550 3200 5550
Wire Wire Line
	3300 3450 3300 5550
Connection ~ 3300 3450
Wire Wire Line
	2800 2300 3550 2300
Connection ~ 3550 3450
Wire Wire Line
	3550 4900 3550 4950
Wire Wire Line
	3550 3750 3550 3800
Wire Wire Line
	3550 2600 3550 2650
Connection ~ 3550 2300
Wire Wire Line
	4050 2600 4050 2950
Wire Wire Line
	4000 3450 4050 3450
Connection ~ 3550 4600
Connection ~ 4050 3450
Wire Wire Line
	4600 3700 4600 3650
Wire Wire Line
	5000 3350 5050 3350
Wire Wire Line
	5050 3350 5050 3300
Wire Wire Line
	5050 3000 5050 2950
Wire Wire Line
	5050 2950 4050 2950
Connection ~ 4050 2950
Connection ~ 4050 2300
Connection ~ 4050 4600
Wire Wire Line
	4800 4600 5250 4600
Wire Wire Line
	5250 2600 5250 3350
Wire Wire Line
	4800 2300 5250 2300
Connection ~ 5250 3350
Connection ~ 5050 3350
Wire Wire Line
	5650 3350 5750 3350
Wire Wire Line
	5700 3150 5850 3150
Wire Wire Line
	5700 3150 5700 3450
Connection ~ 5250 2300
Connection ~ 5250 4600
Wire Wire Line
	6450 3250 6500 3250
Wire Wire Line
	6500 2950 6500 2900
Wire Wire Line
	6500 2900 5750 2900
Wire Wire Line
	5750 2900 5750 3350
Connection ~ 5750 3350
Wire Wire Line
	7300 2400 7500 2400
Wire Wire Line
	7500 2700 7500 2750
Wire Wire Line
	7500 2750 6700 2750
Wire Wire Line
	6700 2750 6700 2500
Connection ~ 7500 2750
Wire Wire Line
	7250 4700 7450 4700
Wire Wire Line
	7450 5000 7450 5050
Wire Wire Line
	7450 5050 6650 5050
Wire Wire Line
	6650 5050 6650 4800
Connection ~ 7450 5050
Wire Wire Line
	7500 3100 7500 3150
Wire Wire Line
	7450 5400 7450 5450
Wire Wire Line
	4600 2900 4600 3050
Wire Wire Line
	5050 5200 5050 5250
Wire Wire Line
	5050 5250 5300 5250
Wire Wire Line
	5300 5250 5300 5300
Connection ~ 5050 5250
Wire Wire Line
	5300 5600 5300 5650
Wire Wire Line
	5050 5600 5050 5650
Wire Wire Line
	4850 5650 5050 5650
Connection ~ 5050 5650
Connection ~ 5300 5650
Wire Wire Line
	4850 5650 4850 5700
Wire Wire Line
	5050 6000 5050 6050
Wire Wire Line
	5050 6050 5300 6050
Wire Wire Line
	5300 6000 5300 6050
Connection ~ 5300 6050
Wire Wire Line
	6550 3250 6550 3500
Wire Wire Line
	6550 3500 7600 3500
Connection ~ 6500 3250
Connection ~ 7450 4700
Connection ~ 7500 2400
Wire Wire Line
	6900 2700 6900 2800
Wire Wire Line
	6050 2850 6050 2950
Wire Wire Line
	6900 2050 6900 2100
Wire Wire Line
	6850 4350 6850 4400
Wire Wire Line
	8350 3350 8150 3350
Wire Wire Line
	8150 3350 8150 3700
Wire Wire Line
	8150 3700 8350 3700
Wire Wire Line
	8150 4100 8350 4100
Wire Wire Line
	8350 4050 8350 4100
Connection ~ 8150 3700
Connection ~ 8350 4100
Wire Wire Line
	4350 3500 4350 3250
Wire Wire Line
	4350 3250 4400 3250
Wire Wire Line
	6050 3650 6050 3550
Wire Wire Line
	6850 5000 6850 5150
Wire Wire Line
	7900 3500 7950 3500
Wire Wire Line
	7950 3500 7950 3150
Wire Wire Line
	7950 3150 8200 3150
Wire Wire Line
	7900 2400 8000 2400
Wire Wire Line
	8000 2400 8000 3850
Wire Wire Line
	8000 3850 8200 3850
Wire Wire Line
	7850 4700 8050 4700
Wire Wire Line
	8050 4700 8050 3500
Wire Wire Line
	8050 3500 8200 3500
Wire Wire Line
	3300 3450 3550 3450
Wire Wire Line
	3550 3450 3700 3450
Wire Wire Line
	3550 2300 4050 2300
Wire Wire Line
	3550 4600 4050 4600
Wire Wire Line
	4050 3450 4050 4300
Wire Wire Line
	4050 3450 4400 3450
Wire Wire Line
	4050 2950 4050 3450
Wire Wire Line
	4050 2300 4500 2300
Wire Wire Line
	4050 4600 4500 4600
Wire Wire Line
	5250 3350 5350 3350
Wire Wire Line
	5250 3350 5250 4300
Wire Wire Line
	5050 3350 5250 3350
Wire Wire Line
	5250 2300 6700 2300
Wire Wire Line
	5250 4600 6650 4600
Wire Wire Line
	5750 3350 5850 3350
Wire Wire Line
	7500 2750 7500 2800
Wire Wire Line
	7450 5050 7450 5100
Wire Wire Line
	5050 5250 5050 5300
Wire Wire Line
	5050 5650 5050 5700
Wire Wire Line
	5050 5650 5300 5650
Wire Wire Line
	5300 5650 5300 5700
Wire Wire Line
	5300 6050 5300 6100
Wire Wire Line
	6500 3250 6550 3250
Wire Wire Line
	7450 4700 7550 4700
Wire Wire Line
	7500 2400 7600 2400
Wire Wire Line
	8150 3700 8150 4100
Wire Wire Line
	8350 4100 8350 4150
$EndSCHEMATC
