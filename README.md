RGB to Component/S-Video/Composite Converter
============================================

This is a further development of the RGB-to-component converter I built
about three years ago:

https://upverter.com/salfter/365dcb155a88fcf1/rgb2component/

This version replaces the two MAX4451 dual op-amps with one MAX4383 quad
op-amp; specs for the two are similar, so performace should likewise be
similar.  The rather large can-type electrolytic capacitors are also
replaced with smaller tantalum capacitors.

More significantly, the new design includes a Sony CXA2075 video encoder so
it can produce S-video and composite output as well.  I've found support for
240p component video in newer displays to be somewhat iffy; perhaps 240p
over S-video will be more widely usable.  There should be no to minimal
degradation of sharpness with S-video vs. component video.  Composite video
will be the usual hot mess, but since the chip supports it, we might as well
implement it.

This board is intended to plug into the RGB output jack of an Apple IIGS. 
If you want to use it with something else, you can either rework the DB-15
connector end to something more suitable for your application or you can
build a cable that adapts your application to the IIGS pinout: red on pin 2,
green on pin 5, blue on pin 9, composite sync on pin 3, +12V on pin 8, -5V
on pin 7, and ground on pins 1, 6, and 13.

The board layout supports both NTSC and PAL, and is configured for NTSC by
default.  Configuring it for PAL requires cutting the NTSC jumper (marked as
such), bridging the PAL jumper, and replacing the crystal and one resistor
with parts with different values as described in the schematic.