EESchema Schematic File Version 4
LIBS:rgb2svid-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 3
Title "RGB to Component/S-Video/Composite Converter"
Date "2018-02-25"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6500 3200 2    60   Output ~ 0
XOUT
$Comp
L rgb2svid-rescue:74HCT04 U13
U 1 1 5A933EA8
P 5150 3200
F 0 "U13" H 5300 3300 50  0000 C CNN
F 1 "74HCT04" H 5350 3100 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 3200 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 5150 3200 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5150 3200
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:74HCT04 U13
U 6 1 5A933F45
P 6050 3200
F 0 "U13" H 6200 3300 50  0000 C CNN
F 1 "74HCT04" H 6250 3100 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 6050 3200 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 6050 3200 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	6    6050 3200
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:74HCT04 U13
U 2 1 5A933FC8
P 5150 4650
F 0 "U13" H 5300 4750 50  0000 C CNN
F 1 "74HCT04" H 5350 4550 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 4650 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 5150 4650 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	2    5150 4650
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:74HCT04 U13
U 3 1 5A934012
P 5150 5000
F 0 "U13" H 5300 5100 50  0000 C CNN
F 1 "74HCT04" H 5350 4900 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 5000 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 5150 5000 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	3    5150 5000
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:74HCT04 U13
U 4 1 5A934049
P 5150 5350
F 0 "U13" H 5300 5450 50  0000 C CNN
F 1 "74HCT04" H 5350 5250 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 5350 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 5150 5350 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	4    5150 5350
	1    0    0    -1  
$EndComp
$Comp
L rgb2svid-rescue:74HCT04 U13
U 5 1 5A934083
P 5150 5700
F 0 "U13" H 5300 5800 50  0000 C CNN
F 1 "74HCT04" H 5350 5600 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 5700 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT04.pdf" H 5150 5700 50  0001 C CNN
F 4 "SOIC-14" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "IC INVERTER 6CH 6-INP 14SO" H 0   0   50  0001 C CNN "Description"
F 6 "Nexperia USA Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 7 "74HCT04D,652" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1727-3798-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	5    5150 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 5A934378
P 4700 5000
F 0 "#PWR045" H 4700 4750 50  0001 C CNN
F 1 "GND" H 4700 4850 50  0000 C CNN
F 2 "" H 4700 5000 50  0001 C CNN
F 3 "" H 4700 5000 50  0001 C CNN
	1    4700 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR046
U 1 1 5A934398
P 4700 5350
F 0 "#PWR046" H 4700 5100 50  0001 C CNN
F 1 "GND" H 4700 5200 50  0000 C CNN
F 2 "" H 4700 5350 50  0001 C CNN
F 3 "" H 4700 5350 50  0001 C CNN
	1    4700 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR047
U 1 1 5A9343B8
P 4700 5700
F 0 "#PWR047" H 4700 5450 50  0001 C CNN
F 1 "GND" H 4700 5550 50  0000 C CNN
F 2 "" H 4700 5700 50  0001 C CNN
F 3 "" H 4700 5700 50  0001 C CNN
	1    4700 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5A9343E1
P 5150 2550
F 0 "Y1" H 5150 2700 50  0000 C CNN
F 1 "3.579545 MHz" H 5150 2400 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 5150 2550 50  0001 C CNN
F 3 "http://www.foxonline.com/pdfs/C4SD.pdf" H 5150 2550 50  0001 C CNN
F 4 "CRYSTAL 3.579545MHZ 18PF SMD" H 0   0   50  0001 C CNN "Description"
F 5 "HC-49/US" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Fox Electronics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "FC4SDCBLF3.579545-T1" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "631-1003-2-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=631-1003-2-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5150 2550
	1    0    0    -1  
$EndComp
Text Notes 4850 3000 0    50   ~ 0
(NTSC; substitute\n4.433619 MHz\nfor PAL)
Wire Wire Line
	5000 2550 4700 2550
Wire Wire Line
	4700 2550 4700 3200
Wire Wire Line
	5300 2550 5600 2550
Wire Wire Line
	5600 2550 5600 3200
$Comp
L proper-passives:R R26
U 1 1 5A93457D
P 5150 3550
F 0 "R26" V 5230 3550 50  0000 C CNN
F 1 "4.7M" V 5050 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5080 3550 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RMCF_RMCP.pdf" H 5150 3550 50  0001 C CNN
F 4 "Stackpole Electronics Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RMCF0805FT4M70" H 0   0   50  0001 C CNN "Mfr PN"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "RMCF0805FT4M70CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=RMCF0805FT4M70CT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 9 "RES 4.7M OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
F 10 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5150 3550
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:C C18
U 1 1 5A934605
P 4900 3850
F 0 "C18" V 4950 3900 50  0000 L CNN
F 1 "33p" V 4850 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4900 3850 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C330JBANNNC.jsp" H 4900 3850 50  0001 C CNN
F 4 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "CAP CER 33PF 50V C0G/NP0 0805" H 0   0   50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21C330JBANNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1105-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1105-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4900 3850
	0    -1   -1   0   
$EndComp
$Comp
L proper-passives:C C19
U 1 1 5A93472B
P 5400 3850
F 0 "C19" V 5450 3900 50  0000 L CNN
F 1 "33p" V 5350 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5400 3850 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C330JBANNNC.jsp" H 5400 3850 50  0001 C CNN
F 4 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 5 "CAP CER 33PF 50V C0G/NP0 0805" H 0   0   50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21C330JBANNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1105-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1105-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5400 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 3550 5000 3550
Connection ~ 4700 3200
Wire Wire Line
	5600 3550 5300 3550
Connection ~ 5600 3200
Wire Wire Line
	4700 3850 4750 3850
Connection ~ 4700 3550
Wire Wire Line
	5050 3850 5150 3850
Wire Wire Line
	5600 3850 5550 3850
Connection ~ 5600 3550
$Comp
L power:GND #PWR048
U 1 1 5A93485E
P 5150 4000
F 0 "#PWR048" H 5150 3750 50  0001 C CNN
F 1 "GND" H 5150 3850 50  0000 C CNN
F 2 "" H 5150 4000 50  0001 C CNN
F 3 "" H 5150 4000 50  0001 C CNN
	1    5150 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4000 5150 3850
Connection ~ 5150 3850
$Comp
L power:GND #PWR049
U 1 1 5A934A22
P 4700 4650
F 0 "#PWR049" H 4700 4400 50  0001 C CNN
F 1 "GND" H 4700 4500 50  0000 C CNN
F 2 "" H 4700 4650 50  0001 C CNN
F 3 "" H 4700 4650 50  0001 C CNN
	1    4700 4650
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:C C20
U 1 1 5A935263
P 6200 3900
F 0 "C20" H 6050 4000 50  0000 L CNN
F 1 "0.1u" H 6250 3800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6200 3900 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F104ZBCNNNC.jsp" H 6200 3900 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V Y5V 0805" H 0   0   50  0001 C CNN "Description"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CL21F104ZBCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=1276-1007-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6200 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 5A9353FF
P 6200 4050
F 0 "#PWR050" H 6200 3800 50  0001 C CNN
F 1 "GND" H 6200 3900 50  0000 C CNN
F 2 "" H 6200 4050 50  0001 C CNN
F 3 "" H 6200 4050 50  0001 C CNN
	1    6200 4050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR051
U 1 1 5A935422
P 6200 3750
F 0 "#PWR051" H 6200 3600 50  0001 C CNN
F 1 "VCC" H 6200 3900 50  0000 C CNN
F 2 "" H 6200 3750 50  0001 C CNN
F 3 "" H 6200 3750 50  0001 C CNN
	1    6200 3750
	1    0    0    -1  
$EndComp
NoConn ~ 5600 4650
NoConn ~ 5600 5000
NoConn ~ 5600 5350
NoConn ~ 5600 5700
Wire Wire Line
	4700 3200 4700 3550
Wire Wire Line
	5600 3200 5600 3550
Wire Wire Line
	4700 3550 4700 3850
Wire Wire Line
	5600 3550 5600 3850
Wire Wire Line
	5150 3850 5250 3850
$EndSCHEMATC
